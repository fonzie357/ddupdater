package de.fonzie.digitaldistribution.updater;

import java.io.IOException;

import javax.swing.JOptionPane;

import org.springframework.context.event.SimpleApplicationEventMulticaster;

import com.formdev.flatlaf.FlatDarkLaf;

import de.fonzie.digitaldistribution.updater.data.AppProperties;
import de.fonzie.digitaldistribution.updater.data.AutoUpdate;
import de.fonzie.digitaldistribution.updater.ui.UpdaterDialog;
import de.fonzie.digitaldistribution.updater.ui.UpdaterEvent;

public class DdUpdaterApplication {
	
	private static UpdaterDialog dialog;
	private static SimpleApplicationEventMulticaster events;

	public static void main(String[] args) {
		
		loadProperties();
		
		FlatDarkLaf.setup();
		
		events = new SimpleApplicationEventMulticaster();
		dialog = new UpdaterDialog();
		events.addApplicationListener(dialog);
		if(checkForUpdate()) {
			runLauncher();
		}
		System.exit(0);
	}

	private static void runLauncher() {
		events.multicastEvent(new UpdaterEvent(dialog, "Starting Launcher", 0));
		try {
			Runtime.getRuntime().exec(AppProperties.getAppProperties().getProperty("launcherCMD"));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(dialog, "Could not start Launcher", "Error", JOptionPane.ERROR_MESSAGE);
		}
		events.multicastEvent(new UpdaterEvent(dialog, "Starting Launcher", 100));
		System.exit(0);
	}

	private static boolean checkForUpdate() {
		return new AutoUpdate(dialog, events).update();
	}

	private static void loadProperties() {
		new AppProperties();
	}
}
