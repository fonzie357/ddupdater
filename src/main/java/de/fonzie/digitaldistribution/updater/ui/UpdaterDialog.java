package de.fonzie.digitaldistribution.updater.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import org.springframework.context.ApplicationListener;

public class UpdaterDialog extends JDialog implements ApplicationListener<UpdaterEvent> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JProgressBar progress;
	private JLabel lblInfo;
	private JPanel infoPane;

	/**
	 * Create the dialog.
	 */
	public UpdaterDialog() {
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

		getContentPane().add(contentPanel, BorderLayout.CENTER);

		infoPane = new JPanel();
		getContentPane().add(infoPane, BorderLayout.CENTER);
		infoPane.setLayout(new BorderLayout(5, 5));

		lblInfo = new JLabel();
		lblInfo.setBorder(new EmptyBorder(10, 10, 0, 0));
		lblInfo.setMinimumSize(new Dimension(400, 20));
		infoPane.add(lblInfo, BorderLayout.NORTH);
		
		progress = new JProgressBar(0, 100);
		progress.setBorder(new EmptyBorder(0, 10, 10, 10));
		progress.setValue(0);
		infoPane.add(progress, BorderLayout.CENTER);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(ABORT);
			}
		});

		init();
	}

	private void init() {
		setTitle("AutoUpdater");
		setBounds(100, 100, 450, 200);
		setMinimumSize(new Dimension(450, 100));
		setLocationRelativeTo(null);
		setVisible(true);
		pack();
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	}

	@Override
	public void onApplicationEvent(UpdaterEvent event) {
		if(event.isNewTask()) {
			lblInfo.setText(event.getCurrentTask());
			progress.setValue(event.getCurrentProgress());
		} else {
			if(progress != null) {
				progress.setValue(event.getCurrentProgress());
			}
		}
	}
}
