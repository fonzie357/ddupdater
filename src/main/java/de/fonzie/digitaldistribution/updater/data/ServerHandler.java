package de.fonzie.digitaldistribution.updater.data;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import de.fonzie.digitaldistribution.updater.entity.Launcher;
import de.fonzie.digitaldistribution.updater.ui.UpdaterEvent;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ServerHandler {

	private String foundURL;

	private SimpleApplicationEventMulticaster events;

	public ServerHandler(SimpleApplicationEventMulticaster events) {
		this.events = events;
	}

	public Launcher findServer() {
		events.multicastEvent(new UpdaterEvent(this, "Connecting to Masterserver", 0));
		String[] urls = AppProperties.getAppProperties().getProperty("domainList").split(",");
		for (int i = 0; i < urls.length; i++) {
			events.multicastEvent(new UpdaterEvent(this, (i + 1) * 100 / urls.length));
			try {
				events.multicastEvent(new UpdaterEvent(this,
						AppProperties.getAppProperties().getProperty("domainList").split(",").length / 100));
				ResponseEntity<Launcher> responseEntity = restTemplate().getForEntity(
						urls[i] + AppProperties.getAppProperties().getProperty("versionEndpoint"), Launcher.class);
				if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
					foundURL = urls[i];
					events.multicastEvent(new UpdaterEvent(this, 100));
					return responseEntity.getBody();
				}
			} catch (Exception e) {
				log.warn("failed to contact masterserver: " + urls[i], e);
			}
		}
		return null;
	}

	public boolean downloadUpdate(String version) {
		events.multicastEvent(new UpdaterEvent(this, "Downloading update", 0));
		try {
			return restTemplate().execute(foundURL
					+ AppProperties.getAppProperties().getProperty("fileEndpoint").replace("{version}", version),
					HttpMethod.GET, null, clientHttpResponse -> {
						Path filePath = Paths.get("tempData", "data.zip");
						Files.createDirectories(filePath.getParent());
						InputStream in = clientHttpResponse.getBody();
						try (FileOutputStream outputStream = new FileOutputStream(filePath.toFile())) {
							long byteCount = 0;
							byte[] buffer = new byte[1024];
							int bytesRead;
							int lastPercent = 0;
							while ((bytesRead = in.read(buffer)) != -1) {
								outputStream.write(buffer, 0, bytesRead);
								byteCount += bytesRead;
								int percentage = (int) ((byteCount
										/ (float) clientHttpResponse.getHeaders().getContentLength()) * 100);
								if (percentage > lastPercent) {
									events.multicastEvent(new UpdaterEvent(this, percentage));
									lastPercent = percentage;
								}
							}
							outputStream.flush();
							in.close();
						} catch (IOException e) {
							log.error("Error while loading file", e);
							return false;
						}
						events.multicastEvent(new UpdaterEvent(this, 100));
						return true;
					});
		} catch (Exception e) {
			log.warn("failed to download from masterserver", e);
		}
		return false;
	}

	public RestTemplate restTemplate() {
		return new RestTemplateBuilder()
				.basicAuthentication(AppProperties.getAppProperties().getProperty("defaultUser"),
						AppProperties.getAppProperties().getProperty("defaultPassword"))
				.build();
	}
}
