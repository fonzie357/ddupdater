package de.fonzie.digitaldistribution.updater.ui;

import org.springframework.context.ApplicationEvent;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UpdaterEvent extends ApplicationEvent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String currentTask;
	private boolean newTask;
	private int currentProgress;

	public UpdaterEvent(Object source, String task, int startProgress) {
		super(source);
		this.currentTask = task;
		newTask = true;
		currentProgress = startProgress;
	}
	
	public UpdaterEvent(Object source, int progress) {
		super(source);
		this.currentProgress = progress;
	}
}
