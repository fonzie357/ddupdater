package de.fonzie.digitaldistribution.updater.data;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import lombok.Getter;

public class AppProperties {

	@Getter
	private static Properties appProperties;

	private static String appConfigPath = "./update.properties";

	public AppProperties() {
		appProperties = new Properties();

		try (FileInputStream in = new FileInputStream(appConfigPath)) {
			appProperties.load(in);
		} catch (Exception e) {
			loadDefaults();
			saveProperties();
		}
	}

	public static void saveProperties() {
		try (FileWriter writer = new FileWriter(appConfigPath)) {
			appProperties.store(writer, "AutoUpdater Properties");
		} catch (IOException e) {
			// Do nothing
		}
	}

	private void loadDefaults() {
		appProperties.setProperty("domainList", "http://localhost:8080/");
		appProperties.setProperty("defaultUser", "updater");
		appProperties.setProperty("defaultPassword", "1234");
		appProperties.setProperty("localVersion", "1-pre");
		appProperties.setProperty("versionEndpoint", "launcher/latestversion");
		appProperties.setProperty("fileEndpoint", "launcher/{version}/get");
		appProperties.setProperty("launcherCMD", "./data/start.exe");
	}
}
