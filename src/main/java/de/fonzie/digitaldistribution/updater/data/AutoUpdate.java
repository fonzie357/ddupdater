package de.fonzie.digitaldistribution.updater.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.Runtime.Version;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.JOptionPane;

import org.springframework.context.event.SimpleApplicationEventMulticaster;

import de.fonzie.digitaldistribution.updater.entity.Launcher;
import de.fonzie.digitaldistribution.updater.ui.UpdaterDialog;
import de.fonzie.digitaldistribution.updater.ui.UpdaterEvent;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AutoUpdate {

	private Version localVersion;
	private Version remoteVersion;
	
	private ServerHandler serverHandler;
	private Launcher remoteInfos;
	
	private UpdaterDialog dialog;
	
	private SimpleApplicationEventMulticaster events;

	public AutoUpdate(UpdaterDialog dialog, SimpleApplicationEventMulticaster events) {
		localVersion = Version.parse(AppProperties.getAppProperties().getProperty("localVersion"));
		this.events = events;
		serverHandler = new ServerHandler(events);
		this.dialog = dialog;
	}

	public boolean update() {
		remoteInfos = findRemoteServer();

		if (remoteInfos == null) {
			JOptionPane.showMessageDialog(dialog, "Could not contact Masterserver", "Error", JOptionPane.ERROR_MESSAGE);
			log.error("Couldn't contact masterserver");
			return false;
		}

		remoteVersion = getRemoteVersion();

		if (localVersion.compareTo(remoteVersion) < 0) {
			if (updateFiles()) {
				updateProperties();
				return true;
			}
		} else {
			return true;
		}
		log.error("Update failed");
		return false;
	}

	private void updateProperties() {
		AppProperties.getAppProperties().put("localVersion", remoteVersion.toString());
		AppProperties.getAppProperties().put("launcherCMD", remoteInfos.getRunCommand());
		AppProperties.saveProperties();
	}

	private boolean updateFiles() {
		log.info("Downloading");
		return serverHandler.downloadUpdate(remoteVersion.toString()) && extractUpdate();
	}

	private boolean extractUpdate() {
		events.multicastEvent(new UpdaterEvent(this, "Installing update", 0));
		
		Path filePath = Paths.get("tempData", "data.zip");
		try (FileInputStream is = new FileInputStream(filePath.toFile());) {
			long contentLength = Files.size(filePath);
			byte[] buffer = new byte[1024];

			FileChannel channel = is.getChannel();
			ZipInputStream zis = new ZipInputStream(is, Charset.forName("Cp437"));
			ZipEntry zipEntry = zis.getNextEntry();

			int lastPercent = 0;

			while (zipEntry != null) {
				File newFile = newFile(Paths.get(remoteInfos.getExtractDestination()).toFile(), zipEntry);
				if (zipEntry.isDirectory()) {
					if (!newFile.isDirectory() && !newFile.mkdirs()) {
						throw new IOException("Failed to create directory " + newFile);
					}
				} else {
					// fix for Windows-created archives
					File parent = newFile.getParentFile();
					if (!parent.isDirectory() && !parent.mkdirs()) {
						throw new IOException("Failed to create directory " + parent);
					}

					// write file content
					FileOutputStream fos = new FileOutputStream(newFile);
					int len;
					while ((len = zis.read(buffer)) > 0) {
						fos.write(buffer, 0, len);
						int percentage = (int) ((channel.position() / (float) contentLength) * 100);
						if (percentage > lastPercent) {
							events.multicastEvent(new UpdaterEvent(this, percentage));
							lastPercent = percentage;
						}
					}
					fos.close();
				}
				zipEntry = zis.getNextEntry();

			}
			zis.closeEntry();
			zis.close();
		} catch (IOException e) {
			log.error("Error while unpacking file: " + filePath.toString(), e);
			return false;
		}
		
		cleanup();
		events.multicastEvent(new UpdaterEvent(this, 100));
		return true;
	}
	
	private void cleanup() {
		log.info("Cleanup");
		try {
			Files.delete(Paths.get("./temp"));
		} catch (IOException e) {
			//Do nothing
		}
	}

	private Version getRemoteVersion() {
		return Version.parse(remoteInfos.getVersion());
	}

	private Launcher findRemoteServer() {
		return serverHandler.findServer();
	}
	
	private static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
		File destFile = new File(destinationDir, zipEntry.getName());

		String destDirPath = destinationDir.getCanonicalPath();
		String destFilePath = destFile.getCanonicalPath();

		if (!destFilePath.startsWith(destDirPath + File.separator)) {
			throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
		}

		return destFile;
	}
}
