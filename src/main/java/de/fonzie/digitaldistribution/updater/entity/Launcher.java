package de.fonzie.digitaldistribution.updater.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "launcher")
@Data
public class Launcher {
	
	@Id
	private String version;
	
	private String extractDestination = "./data";
	
	private String runCommand = extractDestination + "/LANpowered.exe";
	
	@JsonIgnore
	private String dataPath = "./data/launcher/" + version + "/data.zip";
	
	@JsonIgnore
	private boolean published = false;
}
